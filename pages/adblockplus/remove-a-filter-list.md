title=Remove a filter list
description=Remove a filter list that you previously added to Adblock Plus.
template=article
product_id=abp
category=Customization & Settings

You may want to remove a filter list to unblock certain items, or because the filter list is no longer relevant to your browsing habits.

<section class="platform-chrome" markdown="1">
## Chrome

1. From the Chrome toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click the **Trash** icon next to the filter list that you want to remove.
4. Close the tab.
</section>

<section class="platform-msedge" markdown="1">
## Edge

1. From the Edge toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click the **Trash** icon next to the filter list that you want to remove.
4. Close the tab.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

1. From the Firefox toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click the **Trash** icon next to the filter list that you want to remove.
4. Close the tab.
</section>

<section class="platform-opera" markdown="1">
## Opera

1. From the Opera toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click the **Trash** icon next to the filter list that you want to remove.
4. Close the tab.
</section>

<section class="platform-samsungBrowser" markdown="1">
## Adblock Plus for Samsung Internet

1. Open the Adblock Plus for Samsung Internet app.
2. Tap **Configure your filter lists**.
3. From the *Active for websites in* section, clear the check box next to the filter list you want to remove.
4. Tap the back button to refresh.
5. Close the app.
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click the **Trash** icon next to the filter list that you want to remove.
4. Close the tab.
</section>
