title=Remove a website from the whitelist
description=No longer want to allow ads on websites that you previously whitelisted? Remove the websites from your Adblock Plus whitelist.
template=article
product_id=abp
category=Customization & Settings
include_heading_level=h2

Follow the steps below if you added a website to the Adblock Plus whitelist, but now want to remove it.

<? include adblockplus/removewhitelist ?>
