title=Donations for Adblock Plus
description=If you’ve made a donation to support the Adblock Plus project or you’re considering it, you can find some useful information on the topic in this article.
template=article
product_id=abp
category=Popular Topics
popular=true
hide_browser_selector=true

If you’ve made a donation to support the Adblock Plus project or you’re considering it, you can find some useful information on the topic in this article.

## What donation methods are accepted?

We currently accept donations through PayPal or with a Visa, Mastercard or American Express card.

## Is my payment information secure?

In short - yes.

We use PayPal and Stripe as our providers in order to accept donations you make. You can find out more in our [Privacy Policy](https://adblockplus.org/privacy). Rest assured - we don’t have access to sensitive information such as your full card number!

## What’s the refund policy for donations?

If you’ve changed your mind within 120 days of payment, you can write to us at [support@adblockplus.org](mailto:support@adblockplus.org) and we’ll issue you a full refund. Regardless of whether you’ve donated with PayPal or a card, please include the following information:

- Date of transaction
- Amount (found on your email receipt from PayPal or Stripe)
- The email address you used for the transaction
- The transaction code

<aside class="alert warning" markdown="1">
**Note**: We will never ask you for sensitive information regarding your transaction (e.g. your full card number) and you should never provide this either.
</aside>

It’s always extra helpful if you include a screenshot of the receipt you’ve received from PayPal or Stripe but please block any information you might deem sensitive.

Once we’ve received all the necessary information from you, we will issue you a refund. Refunds are processed every week on Wednesdays, with the exception of bank holidays. Please note that we will issue your refund to the original payment method and we cannot make a transfer to another bank account. Rest assured, if your card has expired in the meantime, the funds will still be sent to the original bank account.

On very rare occasions when there are a lot of inquiries to our Support team, this process might experience small delays. Please be sure that we’re doing everything we can to fulfill your request as soon as we can!

## I haven’t received my refund yet!

We process refunds every week on Wednesday, except for bank holidays. That means if you’ve asked for a refund on a Thursday, we will process your request the next Wednesday. You should then see the refund in your account within 5-10 business days, but this depends on the bank or provider.

In some cases where the refund was issued not long after a donation was made, the refund might be processed as a reversal. This means that the original charge will be dropped off from your bank statement altogether as if it never occurred, and your bank account balance will adjust to credit that money back without a specific line item on the statement.

## How can I cancel my monthly subscription?

### PayPal donations

You can manage or cancel a PayPal subscription directly from your PayPal account. To cancel a recurring donation for Adblock Plus, please follow the step-by-step instructions in PayPal’s Help Center.

### Card donations

If you’ve signed up for a recurring donation for Adblock Plus and would like to cancel it, just write us an email at support@adblockplus.org. Please include the following information:

- Date of transaction
- Amount (found on your email receipt from PayPal or Stripe)
- The email address you used for the transaction
- The transaction code

<aside class="alert warning" markdown="1">
**Note**: We will never ask you for sensitive information regarding your transaction (e.g. your full card number) and you should never provide this either.
</aside>

## Can you change my subscription?

For security reasons, we’re not able to modify your subscription. This means that if you would like to adjust the recurring donation amount, the subscription would need to be canceled and a new one needs to be created by you.
