title=Is Adblock Plus the same thing as AdBlock?
description=Adblock Plus is not the same thing as AdBlock.
template=article
product_id=abp
category=Popular Topics
popular=true
hide_browser_selector=true

In short — no. Both are ad blockers, but they are separate projects. Adblock Plus is a version of the original "ad-blocking" project while AdBlock originated in 2009 for Google Chrome.
