title=Adjusting Adblock Browser notifications
description=Set your Adblock Browser for Android notification preferences.
template=article
hide_browser_selector=1
product_id=abb
category=Customization & Settings

1. Open the Adblock Browser app.
2. Tap the **Android menu icon** and select **Settings**.
3. Tap **Notifications**.
<br>Adjust the notification settings to suit your preferences.
